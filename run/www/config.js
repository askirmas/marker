require.config({
    baseUrl: "../../build/",
    paths: {
        "dexie": "../node_modules/dexie/dist/dexie",
        "clipboard": "../node_modules/clipboard/dist/clipboard.min",
        "sha1-min": "../lib/sha1-min"
    }
});
requirejs(['clipboard'], function(Clipboard) {
    new Clipboard('m-rk');
});
requirejs(['main']);