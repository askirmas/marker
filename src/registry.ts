import {IRegistry, mrkGlobal} from './global';

import {goTrajectory, UI} from './html';
import * as text from './text';
import * as StorageTypes from './storage.i';

export default class Registry <T> implements IRegistry <T> {
  private BaseParts : Map<T, {id : number, parts : Set<T>}> = new Map;
  private PartBase : Map<T, {isMarker : boolean, start : number, base : T}> = new Map;
  private Queue = new Map;
  private inRestore : boolean = false;
  public getBaseOf(node : T) : {start : number, base : T} {
    const out = this.PartBase.get(node);
    return out
      ? {start : out.start, base : out.base}
      : null
  }
  public getPartsOf(base : T) : {id : number, parts : Set<T>} {
    return this.BaseParts.get(base)
  }
  public getAllMarkers() : {node : T, start : number}[] {
    return Array.from(this.PartBase)
    .filter(x => x[1].isMarker)
    .map(x => { return {
      'node' : x[0],
      'start' : x[1].start
    }})
  }
  public addBase(base : T, baseMetrics, baseId? : number) {
    if (!this.BaseParts.has(base)) {
      const baseParts = this.BaseParts;
      const queue : ((arg : number) => void)[] = [];
      this.Queue.set(base, queue);
      baseParts.set(base, {parts : new Set});
      const baseRec = baseParts.get(base),
        onId = (id : number) => {
          baseRec.id = id;
          queue.map(x => x(id));
          this.Queue.delete(base);
        };
      if (baseId) onId(baseId)
      else if (!this.inRestore)
        mrkGlobal.storage.putBase(
          new StorageTypes.Base(UI.getCleanUrl(), baseMetrics.trajectory, baseMetrics.metrics, baseId)
        )
        .then(onId)
      }
  }

  public addParts(base : T, parts : T[], offsets : number[]) {
    parts.forEach((part, i) => {
      const isMarker : boolean = this.PartBase.has(part) ? this.PartBase.get(part).isMarker || false : false;
      this.PartBase.set(part, {isMarker, start : offsets[i], base});
      this.BaseParts.get(base).parts.add(part);
    });
  }
  public addMarker(base : T, marked : T, start : number, content : string) {
    const baseId = this.BaseParts.get(base).id;
    this.PartBase.get(marked).isMarker = true;
    if (!this.inRestore) {
      const adding = (id : number) => mrkGlobal.storage.putMarker(new StorageTypes.Marker(id, start, content)); 
      if (baseId) adding(baseId)
      else this.Queue.get(base).push(adding)
    }
  }

  public removePart(part : T, base : T | null = null) {
      if (!base) base = this.PartBase.get(part).base;
      this.PartBase.delete(part);
      if (this.BaseParts.get(base).parts.size === 0) {
        this.BaseParts.delete(base)
      }
  }

  public queueRestore = ({bases, markers} : {bases : StorageTypes.Base[], markers : StorageTypes.Marker[]}) => {
    const q = this.Queue;
    bases.forEach(
      base => q.set(base.id, {trajectory : base.trajectory, metrics : base.metrics, markers : new Map})
    );
    markers.forEach(marker => 
      q.get(marker.baseId).markers.set(marker.start, marker.content)
    );
    this.inRestore = true;
    q.forEach((base, id : number) => {
      if (base.trajectory) {
        const candidate = goTrajectory(base.trajectory),
          content = text.getContentOfTextNode(candidate);
        if (!content || content.length !== base.metrics.length) {
          console.log([base.trajectory, base.metrics]);
        } else {
          // addition checks should be e.i. sha1(content) === base.metrics.sha1
          this.addBase(candidate, base.metrics, id);
          Array.from(base.markers)
          .sort((x, y) => y[0] - x[0])
          .forEach((marker : (number|string)[]) =>
              mrkGlobal.createMarker(candidate, marker[0], marker[0] + marker[1].length, this)
          )
        } 
      }
    });
    this.inRestore = false
  }
}
