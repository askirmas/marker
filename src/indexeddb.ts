import Dexie from 'dexie';
import * as StorageTypes from './storage.i';
export default class IndexedDB extends Dexie implements StorageTypes.IStorage {
  private bases : Dexie.Table<StorageTypes.Base, number>
  private markers : Dexie.Table<StorageTypes.Marker, number>;
  constructor() {
    super('marker');
    const idb = this;
    idb.version(1).stores({
      bases : '++id, &[url+trajectory], url',
      markers : '&[baseId+start], baseId'
    });
    idb.bases.mapToClass(StorageTypes.Base);
    idb.markers.mapToClass(StorageTypes.Marker);
  }
  
  public async loadBaseAndMarkers(url : string) {
    const bases = await this.bases.where('url').equals(url)
    .toArray();
    const markers = await this.markers.where('baseId').anyOf(bases.map(el => el.id))
    .toArray();
    return { bases, markers }
  }
  public async putBase(base : StorageTypes.Base) {
    // if (!('url' in base)) base.url = window.location.hash;    
    return await this.bases.put(base)
  }
  public async putMarker(marker : StorageTypes.Marker) {
    return this.markers.put(marker)   
  }
}
