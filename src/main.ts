import {createGlobal} from './global';

import Registry from './registry';
import IndexedDB from './indexeddb';

import {init, Marker, UI} from './html';

const storage = new IndexedDB,
  registry = <Registry<Node>> new Registry;
createGlobal(document, storage, registry, Marker.create);
storage.loadBaseAndMarkers(UI.getCleanUrl()).then(registry.queueRestore);

/*
const storage = new IndexedDB;
registry.setStorage(storage);
registry.setMarkerConstructor(HtmlMarker.create);
storage.loadBaseAndMarkers(window.location.href).then(registry.queueRestore);
*/
init(registry);

/*const disp = HtmlUI.HtmlUI.addMarkerUrlListener('er', function (command : string) {
  if (command !== '') {
    console.log(command);
  }
  if (self._envReady) self._main();
  self._initReady = true;
});
*/
