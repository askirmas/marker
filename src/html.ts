import {mrkGlobal, IRegistry} from './global';
import * as u from './utils';
import * as text from './text'

export namespace UI {
  export class HotKey {
    public readonly ctrl : boolean = false;
    public readonly meta : boolean = false;
    public readonly shift : boolean = false;
    public readonly alt : boolean = false;
    public readonly key : string;
    constructor(key : string, modifiers : {ctrl? : boolean, meta? : boolean, shift? : boolean, alt? : boolean}) {
      key = key.toUpperCase();
      Object.assign(this, {key}, modifiers);
    }
  }
  export function addHotKeyListener(hotkey : HotKey, handler : () => void) : () => void {
    const fn  = (ev : KeyboardEvent) => {
      let condition = true,
        keys = Object.keys(hotkey),
        i = keys.length - 1;
      while (condition && i >= 0) {
        const prop = keys[i];
        i--;
        condition = prop === 'key'
          ? 'Key' + hotkey.key === ev.code
          : hotkey[prop] === ev[prop + 'Key'];
      }
      if (condition) handler()
    }
    mrkGlobal.root.addEventListener('keydown', fn);
    return () => mrkGlobal.root.removeEventListener('keydown', fn)
  }
  let markerHash : Map<string, string> = new Map;
  const markerHandlers /*: Map<number, (_) => void>*/ = new Map;
  export const markerPrefix = 'mark-';
  const [key, value, keyValue] =
    ((k, v) => [new RegExp(k), new RegExp(v), new RegExp(k + v, 'g')])
    ('[#&]' + markerPrefix + '[^=&]+', '=[^&]+');

  export function sortOutMarkerParams(url : string = window.location.href) : Map<string, string> {
    const fn = (x : string) => x.replace(/#+$/, ''),
      matches = url.match(keyValue),
      clean = new Map;
    return matches ? (
      clean.set('clean',
        fn(matches.reduce(
          (striping, match) => striping.replace(match, match.charAt(0)),
          url
        ))
      ),
      matches.reduce(
        (output, match) => output.set(
          match.match(key)[0].slice(1),
          match.match(value)[0].slice(1)
        ),
        clean
      )
    ) : (
      clean.set('clean', fn(url)), clean
    )
  }

  export function getCleanUrl(leaveNotMarkerHash = false) {
    return leaveNotMarkerHash
      ? window.location.href.replace(/#.*$/, '')
      : markerHash.get('clean') || sortOutMarkerParams(window.location.href).get('clean');
  };
  
  function initMarkerEventing () {
    markerHash = sortOutMarkerParams();
    markerHandlers.forEach((fn : (_ : any) => void) => fn.call(markerHash));
    window.addEventListener("hashchange", (ev) => {
      const newH = sortOutMarkerParams(),
        newClean = newH.get('clean'),
        oldClean = markerHash.get('clean');
      newH.delete('clean');
      markerHash.delete('clean');
      const isEqual = u.maps.equals([newH, markerHash]);
      newH.set('clean', newClean);
      markerHash.set('clean', oldClean);
      if (!isEqual) {
        markerHandlers.forEach((fn : (_ : any) => void) => fn.call(newH, markerHash));
        markerHash = newH;
      }
    }, true);
  }
  export function addMarkerUrlListener (
//      type : string,
    handler : (value : Map<string, string>, previous? : Map<string, string>) => void
  ) : () => void {
    const id = Math.random();
    markerHandlers.set(id, handler);
    handler(markerHash);
    return () => markerHandlers.delete(id)
  }

  function getSelection ()/* : Promise<Range> */{
    const selection = mrkGlobal.root.getSelection();
    return  new Promise((resolve, reject/*?*/) => {
      if (!selection.isCollapsed) resolve(selection.getRangeAt(0))
      else if (reject !== undefined) reject()
    })
  }

  function onSelection () {
    getSelection()
    .then((range : Range) => {
      const [st, end] = [range.startContainer, range.endContainer],
        nodes = u.bottomWalk(st, end, isTextNode);
      
      if (range.startContainer === range.endContainer) {
        const marked = Marker.ed(range.startContainer);
        marked
        ? Marker.destroy(marked)
        : Marker.create(range.startContainer, range.startOffset, range.endOffset);
      }
      range.detach();
    });        
  }
  export function getStartPoint(node : Element) {
    const {top, left} = node.getClientRects()[0]; // or getBoundingClientRect()
    return {top, left}
  }
  export function scrollTo(node : Element) {
    /*const {bottom, top, left, right} = node.getBoundingClientRect(),
      [x, y] = [
        min(left, (left + right + window.innerWidth) / 2),
        min(top, (top + bottom + window.innerHeight) / 2)
      ]
    ;*/
    const {top, left} = node.getBoundingClientRect();
    setTimeout(
      () => window.scrollTo(top, left),
      500
    );
  }
  export function init () {
    initMarkerEventing();    
    addHotKeyListener(new HotKey('m', {ctrl : true}), onSelection);
    mrkGlobal.root.body.addEventListener('dblclick', onSelection);
  }    
}

export function getTrajectory(node : Node) : number[] {
  let pointer = node;
  const trajectory = [];
  while (pointer.parentNode && pointer !== mrkGlobal.root.body) {
    trajectory.push(
      u.indexOf(
        pointer.nodeType === 1
        ? pointer.parentNode.children
        : pointer.parentNode.childNodes,
        pointer
      )
    );
    pointer = <Element>pointer.parentNode;
  }
  return trajectory;
}

export function goTrajectory(trajectory : number[]) : Node {
  let pointer = mrkGlobal.root.body;
  for (let i = trajectory.length - 1; pointer && i >= 0; i--) 
    pointer = (i ? pointer.children : pointer.childNodes)[trajectory[i]];
  return pointer;
}

function isTextNode(node : Node) {
  return 'splitText' in node && 'data' in node
}

function executePeriodicaly(fn : () => boolean, timeout = 100) {
  if (!fn()) window.setTimeout(fn, timeout)
}

export namespace Marker {
  let registry : IRegistry<Node>;
  const tag = 'm-rk';
  export const initArgs = [tag, HTMLSpanElement];
  export function setRegistry(_registry : IRegistry<Node>) {
    registry = _registry;
  }
  export function create (node : Text, start : number, end : number, _registry? : IRegistry<Node>) {
    registry = registry || _registry;
    const parent = node.parentNode,
      marker = mrkGlobal.root.createElement(tag),
      _baseR = registry.getBaseOf(node),
      {base, start : partOffset} = _baseR ? _baseR : (
        registry.addBase(node, text.createBaseMetrics(node)),
        {base : node, start : 0}
      );
    // Something wrong…
    // {start, end} = adjustTextOffsets(node, start, end);
    let [rightPart, content, leftPart] = text.splitText(node, start, end);
    marker.setAttribute('data-clipboard-text', "URL soon… " + content.data);
    marker.appendChild(content);

    if (rightPart && text.getContentOfTextNode(rightPart).length === 0) {
      parent.removeChild(rightPart);
      rightPart = null;
    }
    const marked = marker.firstChild,
      markerContent = text.getContentOfTextNode(marked),
      lpl = text.getContentOfTextNode(leftPart).length,
      offsets = [0, lpl, lpl + markerContent.length].map(x => x + partOffset);

    if (lpl === 0 && base !== leftPart) {
      parent.removeChild(leftPart);
      leftPart = null;
    };
    registry.addParts(base, [leftPart, marked, rightPart], offsets);
    registry.addMarker(base, marked, offsets[1], markerContent);

    if (leftPart.nextSibling) parent.insertBefore(marker, leftPart.nextSibling)
    else parent.appendChild(marker)

    return ;
  };
  export function destroy (marker : Node) : void {
    //if (isHtmlMarker(marker)) return;
    const parent = marker.parentNode,
      marked = marker.firstChild,
      base = registry.getBaseOf(marked).base,
      siblings /*: Set<Node>*/ = registry.getPartsOf(base).parts,
      siblingsStarts /*: Map<Node, number>*/ = new Map();

    siblingsStarts.set(marked, registry.getBaseOf(marked).start);
    let  pointer = marker;
    while (siblings.has(pointer.previousSibling)) {
      pointer = pointer.previousSibling;
      siblingsStarts.set(pointer, registry.getBaseOf(pointer).start);
    }
    pointer = marker;
    while (siblings.has(pointer.nextSibling)) {
      pointer = pointer.nextSibling;
      siblingsStarts.set(pointer, registry.getBaseOf(pointer).start);
    }

    const frag = text.joinTexts(Array.from(siblingsStarts)
    .sort((x, y) => x[1] - y[1])
    .map(x => /*<Text>*/x[0]));
    Array.prototype.forEach.call(frag.childNodes,
      (node : Node) => registry.removePart(node, base)
    );
    parent.replaceChild(frag, marker);
  }  
  const is = (x : any) => 'tagName' in x && (x.tagName || '').toLowerCase() === tag;
  export function ed (node : Node) : Node | null {
    let ans = false,
      pointer = node;
    while (!is(pointer) && pointer && pointer !== mrkGlobal.root.body) {
      pointer = pointer.parentNode;
    }
    return is(pointer) ? pointer : null
  }
}

export function init (registry : IRegistry<Node>) {
  Function.prototype.call(
    mrkGlobal.root.registerElement || customElements.define,
    ...Marker.initArgs
  );
  Marker.setRegistry(registry);
  UI.init();
  MarkerDoc.create(registry);
  UI.addHotKeyListener(new UI.HotKey('m', {alt : true}), MarkerDoc.toggleVisibilityWithRecreation);
}

export namespace MarkerDoc {
  let registry : IRegistry<Node>;
  export let self : HTMLElement;
  export function create(_registry? : IRegistry<Node>) {
    registry = registry || _registry;
    self = mrkGlobal.root.createElement('div');
    self.setAttribute('id', 'markerdoc');
    /*const bg = mrkGlobal.root.createElement('div');
    bg.setAttribute('id', 'markerdocbg');
    self.appendChild(bg);*/
    mrkGlobal.root.body.appendChild(self);
    return self
  }
  export function toggleVisibilityWithRecreation() {
    if (self.classList.contains('visible')) {
      hide();
      detach();
      create();
    } else {
      fill();
      show();
    }
  }
  export function show() { self.classList.add('visible'); }
  export function hide() { self.classList.remove('visible'); }
  export function erase() {
    self.innerHTML = ''
  }
  export function detach() {
    self.parentNode.removeChild(self)
  }  
  export function fill(_registry? : IRegistry<Node>) {
    registry = registry || _registry;
    const marked = document.createElement('span');
    marked.classList.add('marked');
    const comm = document.createElement('span');
    comm.contentEditable = "true";
    comm.classList.add('comm');
    registry.getAllMarkers().map(mrk => Object.assign(mrk, UI.getStartPoint(<Element>mrk.node.parentNode)))
    .sort((mrk1, mrk2) =>
      mrk1.top === mrk2.top
      ? mrk1.start - mrk2.start
      : mrk1.top - mrk2.top
    )
    .forEach(mrk => {
      const mrked = marked.cloneNode();
      const comm_ = comm.cloneNode();
      mrked.appendChild(mrk.node.cloneNode());
      self.appendChild(comm_);
      self.appendChild(mrked)
    });
    self.appendChild(comm);
  }
}