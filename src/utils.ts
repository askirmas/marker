export const indexOf = (ar : any[], x : any) => Array.prototype.indexOf.call(ar, x);
export const min = (x : any, y : any) => x < y ? y : x;
export const max = (x : any, y : any) => x > y ? x : y;
export const call = (x : (...args : any[]) => any) => x();
export const ignore : (...args : any[]) => void = () => {return};
const letters = '[^\s\.,:-–-]+';
export const lettersAt = Object.create(null, {
  begin : { value : new RegExp('^' + letters) },
  end : { value : new RegExp(letters + '$') }
});

export function treeWalk(root : Node, criteria = (x : Node) => false) {
  const walker = document.createTreeWalker(
    root,
    NodeFilter.SHOW_ALL,
    { acceptNode:
      node => criteria(node)
      ? NodeFilter.FILTER_ACCEPT
      : NodeFilter.FILTER_SKIP
    },
    false
  );
  const nodeList : Set<Node> = new Set;
  nodeList.add(walker.currentNode);
  while (walker.nextNode()) nodeList.add(walker.currentNode);
  return Array.from(nodeList)
}

export function bottomWalk(start : Node, end : Node, criteria = (el : Node) => false) : Node[] {
  const leaves : Set<Node> = new Set/*,
    returning : () => Node[] = () => Array.from(leaves.keys())*/;
  let el = end,
    wasStart = false;
  while (!wasStart && el && el !== document.body) {
    if (criteria(el))
      leaves.add(el);
    if (el === start) wasStart = true;
    while (!el.previousSibling) {
      el = el.parentNode;
      if (el === document.body || el === null) return Array.from(leaves.keys()); // returning won't work
    }
    el = el.previousSibling;

    while (!criteria(el) && el.lastChild) el = el.lastChild;
  }
  return Array.from(leaves.keys());
}
export namespace maps {
  export function merge <T1, T2> ([...maps] : Map<T1, T2>[]) : Map<T1, T2> {
    return maps.reduce(
      (output, map) => {
        map.forEach((key, value) => output.set(key, value));
        return output
      },
      new Map
    )
  }
  export function intersect <T1, T2> ([...maps] : Map<T1, T2>[]) : Map<T1, T2> {
    if (maps.length === 0) return new Map;
    const output = merge([maps[0]]);
    let size = output.size;
    if (maps.length > 1) maps.reduce((_a, map) => {
      map.forEach((_, key) => {
        if (output.has(key)) {
          output.delete(key);
          size--;
          if (size === 0) return output;
        }
      });
      return _a
    });
    return output;
  }
  export function equals <T1, T2> ([map, ...maps] : Map<T1, T2>[]) : boolean {
    return maps.length < 1
    ? true
    : merge([map, maps[0]]).size !== map.size
    ? false
    : equals(maps)
  }
}