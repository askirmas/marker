import * as u from './utils';
import {getTrajectory} from './html';
// import {sha1} from 'sha1-min';

function adjustTextOffsets(text : Text, start : number, end : number) {
  if (!text || start >= end) return null;
  const content = getContentOfTextNode(text);
  let matched : string[];
  if (start) {
    matched = content.substring(0, start).match(u.lettersAt.end);
    if (matched) start -= matched[0].length;    
  }
  if (end < content.length - 1) {
    matched = content.substring(end).match(u.lettersAt.begin);
    if (matched) end += matched[0].length;
  }

  return {start, end}
}

export function getContentOfTextNode(text : Text) : string {
  return text && text.data || ''
}
function getMetricsOfTextNode(text : Text) {
  const content = getContentOfTextNode(text);
  return {
    length : content.length/*,
    :(
    //sha1 : sha1(content)*/
  }
}
export function createBaseMetrics(text : Text) {
  return {
    metrics : getMetricsOfTextNode(text),
    trajectory : getTrajectory(text)
  }
}

export function joinTexts(texts : Text[]) {
  if (!texts) return null;
  const frag = document.createDocumentFragment();
  texts.forEach(text => frag.appendChild(text));
  frag.normalize();
  return frag
}

export function splitText(text : Text, start : number, end : number) {
  return [text.splitText(end), text.splitText(start), text]/*.map(x => {
    if (getContentOfTextNode(x).length === 0) {
      x.parentNode.removeChild(x);
      x = null;
    }
    return x
  })*/
}
