export interface IStorage {
  loadBaseAndMarkers : (docUrl : string) => Promise<{ bases : Base[], markers : Marker[]}>;
  putBase : (base : Base) => Promise<number>;
  putMarker : (marker : Marker) => Promise<number>;
}
export class Base {
  public id? : number;
  public url : string;
  public trajectory : number[];
  public metrics : Object;
  constructor(url : string, trajectory : number[], metrics : Object, id? : number) {
    Object.assign(this, {url, trajectory, metrics}, id ? {id} : null);
  }
}

export class Marker {
  public id? : number;
  public baseId : number;
  public start : number;
  public content : string;
  constructor(baseId : number, start : number, content : string, id? : number) {
    Object.assign(this, {baseId, start, content}, id ? {id} : null);
  }  
}
