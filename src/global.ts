export class Global <T1, T2, T3> {
  public root : T1;
  public storage : T2;
  public registry : IRegistry<T1>;
  public createMarker : T3;
  constructor(root : T1, storage : T2, registry : IRegistry<T1>, createMarker : T3) {
    Object.assign(this, {root, storage, registry, createMarker})
  }
}
export interface IRegistry <T> {
  getBaseOf(node : T) : {start : number, base : T};
  getAllMarkers() : {node : T, start : number}[];
  addBase(base : T, ...args : any[]) : void;
  addParts(base : T, parts : T[], offsets : number[]) : void;
  addMarker(base : T, marked : T, 
  ...args : any[]) : void;
  removePart(part : T, base? : T) : void;
  getPartsOf(base : T) : {id : number, parts : Set<T>};
}
export let mrkGlobal : Global;
export function createGlobal<T1, T2, T3>(root : T1, storage : T2, registry : IRegistry<T1>, createMarker : T3) {
  mrkGlobal = new Global(root, storage, registry, createMarker)
}

